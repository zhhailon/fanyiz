## Fanyiz ##
This app provide translation between Chinese and English using [Youdao Fanyi](http://fanyi.youdao.com) API.

You can run `go.sh` to start the app.

Or you can go to [Fanyiz](http://fanyiz.heroku.com) to take a look.
