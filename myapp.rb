# myapp.rb
# encoding: UTF-8
require 'rubygems'
require 'sinatra'
require 'youdao-fanyi'

get '/' do
	erb :index
end

post '/s/' do
	unless request.xhr?
		redirect '/', 303
	end
	words = params[:w].strip
	if words.empty?
		return ""
	end
	YoudaoFanyi::Config.key_from = "youdao-fanyi"
	YoudaoFanyi::Config.key = 1629987369
	result = YoudaoFanyi.search_result_obj words.force_encoding("UTF-8")
        if result.nil?
          return "...blah blah..."
        end
	translation = result.translation
	if translation.instance_of? Array
		translation = translation.join "<br />"
	end
#	log = File.open("log", 'a')
#	log.puts "#{ Time.now } \n #{ request.ip } :\n#{ words }" +
#	"\n========>\n #{ translation }\n+++++++++++++++++++++++++++++++"
#	log.close
	return "#{ translation }"
end

get '/*' do
	@url = request.path
	erb :error_page_404
end

